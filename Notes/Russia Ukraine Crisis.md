# UNSC Resolution Against Russia
- Tabled by Albania and the US, got support from 11 members but got vetoed
- China India and UAE abstained
- Aimed at condemning the aggression by the Russians and aimed to provide Humanitarian aid
- Resolution called after 2 late night meetings on 21 and 23 Feb

# ICJ on Russia Ukraine Crisis
- ICJ headed by Joan Donoghue of the US
- Vienna Convention of 1961 is related to the diplomatic intercourse and immunities, lead to the formation of Vienna Convention on Diplomatic Relations
- The Court begins by recalling that, on 26 February Ukraine filed an application against Russia concerning “a dispute” on the interpretation, application and fulfilment of the Genocide Convention
- According to Ukraine what Russia is doing in its borders is called as Genocide as it is willing to destroy an entire nation and its people which is why a case was tabled under the Genocide convention
- Genocide Convention
	- 1947 : The UNGA accepted the fact that Genocide is also a international crime
	-  In 1948, the Convention on the Prevention and Punishment of the Crime of Genocide was adopted by the UNGA and came into force in 1951.
	- Genocide refers to the acts that are committed with the intent to destroy in whole or partly, a national, ethnical, racial or religious group.
# Why Aggression
-   **Geo-Political and Strategic Factors**
    -   Russia claims that the eastward expansion by the NATO which they call “**enlargement**”, has threatened Russia’s interests and has asked for written security guarantees from NATO.
    -   NATO, led by the U.S., has planned to install missile defence systems in eastern Europe in countries like Poland and the Czech Republic to counter Russia’s intercontinental-range missiles.
- **NATO**
	- Began post WW2 to provide security to Europe
	- Collective Security Arrangement
	- In 1949
	- 30 total countries 28 European and 2 American
	- Enlargement means including a new member 
	- Most issue by Russia because of Article 5

## How Will India be Affected
-   An invasion by Russia would put pressure on India to choose between the Western alliance and Russia.
    -   Maintaining strong relations with Russia serves India’s national interests. India has to retain a strong strategic alliance with Russia as a result, India cannot join any Western strategy aimed at isolating Russia.
-   There is a possibility of [CAATSA](https://byjus.com/free-ias-prep/countering-americas-adversaries-through-sanctions-act-caatsa/) (Countering America’s Adversaries Through Sanctions Act) sanctions on India by the U.S. as a result of the **S-400**
-   A pact between the US and Russia might affect Russia’s relations with China. This might allow India to expand on its efforts to re-establish ties with Russia.
-   The issue with Ukraine is that the world is becoming increasingly economically and geopolitically interconnected. Any improvement in Russia-China ties has ramifications for India.
-   There is also an impact on the strong Indian diaspora present in the region, threatening the lives of thousands of Indian students.

## Indian Stance
- Neutrality is the Word here
-  Immediately after the annexation, India abstained from voting in the [UN General Assembly](https://byjus.com/free-ias-prep/united-nations-general-assembly-unga/) on a resolution that sought to condemn Russia.
-   In 2020, India voted against a Ukraine-sponsored resolution in the UN General Assembly that sought to condemn alleged human rights violations in Crimea.
-   India’s position is largely rooted in neutrality and has adapted itself to the post-2014 status quo on Ukraine.

## Minsk Agreement
-   This was a 13 point agreement that involved representatives from Russia, Ukraine, the Organization for Security and Cooperation in Europe (OSCE) and was signed in 2015.
-   The major aim of the agreement was to end the war in the Donbas region of Ukraine.
-   The agreement intended to take a series of steps with regards to military and political reforms to establish peace in the disputed areas of Donetsk and Luhansk of Ukraine.


## Russian Facts
- Russian President = Vladdy
- Russian Foreign Minister = Sergey Lavrov
- Russian Defense Minister = Sergey Shoygu
- Russian Representation to the UN = Vasily Nebenzya

## Indian Facts
- Indian Prime Minister = NaMo
- Indian Defense Minister = Rajnath Singh
- Indian Representative to the UN = TS Trimurti appointed in May 2020