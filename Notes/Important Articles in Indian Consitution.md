# Important Articles in The Constitution

## Important Parts of the Constitution

The Indian Constitution is divided into 9 parts

| Part | Subject Matter                                 |
|------|------------------------------------------------|
| 1    | Union and Its Territory                        |
| 2    | Citizenship                                    |
| 3    | Fundamental Rights                             |
| 4    | Directive Principles                           |
| 5    | Union                                          |
| 6    | States                                         |
| 7    | Repealed ((Related to the rights of Maharajas) |
| 8    | Union Territories                              |
| 9    | Panchayats                                     |
| 10   | Scheduled and Tribal Areas                     |
| 11   | Relation Between Union and State               |
| 12   | Finance Property and Suits                     |
| 13   | Trade Commerce Intercourse                     |
| 14   | Services Under Union and State                 |
| 15   | Elections                                      |
| 17   | Official Languages                             |
| 18   | Emergency Provisions                           |
| 20   | Amendments to the Constitution                 |
| 21   | Temporary Powers                               |





## Article List

| Article       | Significance                                                                           |
|---------------|----------------------------------------------------------------------------------------|
| Article 2     | Establishment and Establishment of New States                                          |
| Article 3     | Formation of New State and Alteration of Boundries                                     |
| 5, 6, 10, 11  | Citizenship and Rights of Citizenship                                                  |
| 12, 13        | Defination of State and The Laws are void if against Rights                            |
| 14-18         | Equality                                                                               |
| 19-22         | Freedom                                                                                |
| 23-24         | Human Trafficing                                                                       |
| 25-30         | Right to Freedom of Religion                                                           |
| 29-30         | Rights of Minority Institutions to Establish Educational Institutions                  |
| 32            | Remedies for enforcement of fundamental rights                                         |
| 37-51         | DPSP (36 Defination of DPSP and 37 is the Application of DPSP)                         |
| 51A           | Fundamental Duties                                                                     |
| 52            | The President and his Powers                                                           |
| 54            | Election of the President                                                              |
| 61            | Impeachment of the President                                                           |
| 66            | Election of the Vice President                                                         |
| 72            | Pardoning Powers of the President                                                      |
| 79-81         | Constitution of the Parliament                                                         |
| 93            | Speakers and Deputy Speakers                                                           |
| 112           | Financing Budget                                                                       |
| 123           | Ordinances                                                                             |
| 124           | SC                                                                                     |
| 125           | Salaries of the Judges                                                                 |
| 126           | CJi                                                                                    |
| 300A          | Right to Property                                                                      |
| 324           | Power of Election Granted to the CEC                                                   |
| 325           | No person to be ineligable for the electoral roll on the grounds of religion caste etc |
| 153-213       | Powers of the Governor and his Pardoning Powers                                        |
| 214           | High Courts                                                                            |
| 352, 356, 360 | Proclaimation of Emergency, State Emergency and Financial Emergency                    |
| 368           | Power of the Parliament to Amend the Constitution                                      |
| 343           | Hindi as the National Language                                                         |

