#+title: Criminal Law in India

* Indian Penal Code
- Formed in 1860
- It is a code that lays down the defination of criminal acts and the subsequent punishment by law which can be given to those who commit such acts

* What is a Criminal Act
- A criminal act is one which includes criminal or dishonest intention. Any act which is done which such an intension can be termed criminal in nature. This criminal intension is also termed as "mens rea" which is its latin origin
- The Criminal Act includes 2 things
  - Actus Reus or the Criminal Act
  - Mens Rea or the Criminal Mind
- The act that is being committed must be forbidden by law, but along with that it also must be with the intension to harm someone or to cause injury thus leading to ill intention
- Crime also need not successful and can also be the act of preparing for such a criminal act with the ill intension which can ammount to a criminal act in itself
- Also important to note is that there is a difference between *motive and intention*

** Exceptions to Mens Rea
- While the criminal nature of an act can only be proved with the presence of mens rea there are some exceptions where the act does not need intension to be called criminal
- *Strict Liability*
  - Things that harm the public as a whole and lead to a mass impact have a strict liability even thought the act may not be done with a bad intention and was just an accident


* Kinds of Criminal Acts
** Criminal Procedure Code
- CrPC provides machinery for the punishment of offenders against the substantive Criminal Law
- Rules and procedures layed down against any person who has committed an offence under any Criminal Law
- The classification of offences is in CrPC

** Bailable and Non Bailable
- Defined in 2(a)
- Bailable = They are acts where the accused has the rights for bail
- Non Bailable = They are acts where the accused does not have the rights for bail and bail can only be granted by a judge

** Non Bailable Offences
- Murder 302
- Dowry Death 304-B
- Attempt to Murder 307
- Voluntary Causing Greivious Hurt 326
- Kidnapping 363
- Rape 376

** Cognizable and Non Cognizable
- Cognizable = Non Bailable Offences Section 2c 
- Non Cognizable = Bailable Offences Section 2l 

** Compundable and Non Compoundable
- Compundable = Bailable
- Non Compoundable = Non Bailable

* How Does Criminal Trial Take Place
- Investigation
- Inquiry
- Trial

* FIR (First Information Report)
- FIR filed under *154 of CrPC*
- It aim is to set criminal law in motion
- It also has an evidentiary value, it can be used to corroborate the informants under 157 of the Indian Evidence Act

