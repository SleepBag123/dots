# SleepBag123's Dotfiles

Howdy Traveller,
Welcome to my dotfiles repository where I version control and sync all my config files to various programmes that I use everyday. This comes really handy if you like me have been diagnosed with Stage 3 Distro Hopping Syndrome.

![](scrot.png)

## How to use this config ?

Unfortunately all of the dependencies that you need for this config have not been provided in this repo, so you may and will have to go and fetch some stuff off the internet.

Most important of them all being the font that I am using in the foot/alacritty terminal, which is [SFMono Nerd Font](https://github.com/epk/SF-Mono-Nerd-Font)

__List of used programmes__


- __Foot__ = Terminal Emulator 
  
  This is used in my Sway Window Manager setup as the default terminal emulator and you can change this to something else in the Sway config file.
- __Sway__ = Window Manager 
 
  Sway is an i3-wm compatible window manager written natively for wayland, and I have come to prefer this over i3 on Xorg

- __Bemenu__ = Run Launcher 
  
  Bemenu is a wayland replacement for the popular run launcher Dmenu written by the Suckless Guys, I still use dmenu in my X11 setup with i3
  
I have also bundled some configs for my X11 programs

- __Alacritty__ = Terminal Emulator 
  
  Alacritty is my terminal of choice while using X11 and while it can also be used in Wayland I prefer foot in that scenario. It is minimal and GPU accelerated and currently my config uses the Tokyo Night Colorscheme
- __i3-wm__ = Window Manager 
  
  My preferred window manager on X11 is i3 and that is the reason, when I switched to Wayland I chose Sway because it is infact 100 % i3 compatible
  
  NOTE : This config does not contain a bar because I have frankly stopped using them, so you are open to add a bar of your liking to the config
- __dmenu__ = Run Laucher 
  
  Dmenu is my run launcher of choice for my X11 setup and it is one of my most heavily used programs in my setup, though you can always shift to another run launcher such as rofi

## Neovim
I also have in here my [Neovim](https://neovim.io/) config, which is basically tuned to my workflow which is very very markdown heavy as I primarily use it for writing notes, and NO it is not written in Lua because I haven't gotten to it.

One thing to keep in mind is that just placing this ```nvim/``` folder into ```.config/``` wont get the job done you also need to clone [Vim - Plug](https://github.com/junegunn/vim-plug) which is the package manager I am using for my config

## Misc

There is also other stuff in here such as my bin folder which contains the scripts that I use on a daily basis, my zshrc file and Xresources (although I probably dont need that now) file, use it and tweak it to your liking
