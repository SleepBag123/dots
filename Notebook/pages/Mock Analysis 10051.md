- **Test Score = 60**
- Things that went right :
	- The time taken for all the sections was much less than expected and almost all sections were attempted, only some questions in Legal and Quant were not done
	- Comparatively the performance in the 2 worst sections ie QT and CR was much much better than other sections with QT being the most accurate of the lot.
	- A learning here is that while I am able to do less questions in QT the ones that I do usually turn out to be right, thus it is in my favour to make sure that I do all of them
	- The Weekly CA videos came handy as I had better chances at doing CA questions this mock
- Things that did not go right :
	- CR LR, and Eng need a lot of practise still. The accuracy and the marks to the relative number of questions attempted is very low.
	- A lot of my questions are incorrect because I donot make sure to keep in mind the CANNOT and the CAN according to the question, thus when I think I know what the question means I dont and usually the question is just the opposite which I fail to recognise due to the CAN and the CANNOT
	- Donot tend to overthink the most important advice of all and a **repeated mistake** overthinking every question leads to a final answer that is completely wrong and a realisation that the first fluke was correct, and that overthinking lead to the incorrect answer.
	-
-
-