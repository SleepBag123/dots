(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;Change Font

(set-face-attribute 'default nil
  :font "Ubuntu Mono"
  :height 130
  :weight 'medium)
(set-face-attribute 'variable-pitch nil
  :font "Ubuntu Mono"
  :height 130
  :weight 'medium)
(set-face-attribute 'fixed-pitch nil
  :font "Ubuntu Mono"
  :height 130
  :weight 'medium)
;; Makes commented text and keywords italics.
;; This is working in emacsclient but not emacs.
;; Your font must have an italic face available.
(set-face-attribute 'font-lock-comment-face nil
  :slant 'italic)
(set-face-attribute 'font-lock-keyword-face nil
  :slant 'italic)

;; Uncomment the following line if line spacing needs adjusting.
(setq-default line-spacing 0.12)

;; Needed if using emacsclient. Otherwise, your fonts will be smaller than expected.
(add-to-list 'default-frame-alist '("Ubuntu Mono" . "13"))
;; changes certain keywords to symbols, such as lamda!
(setq global-prettify-symbols-mode t)

;; Appearance Changes

(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode 0)

(straight-use-package 'doom-themes)
;; (straight-use-package 'modus-themes)
(straight-use-package 'doom-modeline)
;;(doom-modeline-mode 0)
(load-theme 'doom-nord 1)
;;(custom-set-variables '(inverse-video t))


;; Xah Fly Keysk

;(add-to-list 'load-path "~/.emacs.d/lisp/")
;(require 'xah-fly-keys)
;(xah-fly-keys-set-layout "qwerty")

(straight-use-package 'logos)


;; Stop Creating Temporary Files

(setq make-backup-files nil) ; stop creating backup~ files
(setq auto-save-default nil) ; stop creating #autosave# files

;; SMOCE Stack

;; Selectrum

(straight-use-package 'selectrum)
(selectrum-mode +1)

;; Marginalia

(straight-use-package 'marginalia)
(marginalia-mode 1)

(straight-use-package 'consult)
(straight-use-package 'org-bullets)

(add-hook 'org-mode-hook #'(lambda ()

                             ;; make the lines in the buffer wrap around the edges of the screen.
                             
                             ;; to press C-c q  or fill-paragraph ever again!
                             (visual-line-mode)
			     (org-bullets-mode)))
			     ;;(variable-pitch-mode)))
(setq org-startup-indented t)
(setq org-agenda-files (list "~/dots/Gyan-Kosh/Agenda.org"))

;; XAH FLY KEYS CUSTOMISATION
 

(display-time-mode 1)
(display-battery-mode 1)

;; Magit
(straight-use-package 'magit)
;; My configs for emacs window manager
(straight-use-package 'markdown-mode)

(straight-use-package 'pdf-tools)
(pdf-tools-install)
;; Emacs Mail Client

(setq next-line-add-newlines t)


;;KEYBOARD SHORTCUTS

(global-set-key (kbd "C-,") 'consult-buffer)
(global-set-key (kbd "C-c C-e") 'eval-buffer)
(global-set-key (kbd "C-c d") 'dired-jump)


;;----------------------------------------------------------------------------------------------------------------------;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("8f1eab02c66e5ed476d60a018ffa32ca181fea4be1ee0a5e197b2d7026608a58" "850bb46cc41d8a28669f78b98db04a46053eca663db71a001b40288a9b36796c" "025acf94eb56f1b569f853f77effb1aa5006947a9df3a913f52fcb8daf9a15b2" "1bb8f76bcd04a2b25a663a3da69235fbdbe9db1d5fe7efc6e8fcfc5e1030c9c3" "5b61f8b18975a8c280d495cc2dcd39784cbecf6f0736c623b8cd55beb2fb4629" "cbd8e65d2452dfaed789f79c92d230aa8bdf413601b261dbb1291fb88605110c" "aea30125ef2e48831f46695418677b9d676c3babf43959c8e978c0ad672a7329" "44961a9303c92926740fc4121829c32abca38ba3a91897a4eab2aa3b7634bed4" "f6665ce2f7f56c5ed5d91ed5e7f6acb66ce44d0ef4acfaa3a42c7cfe9e9a9013" "333958c446e920f5c350c4b4016908c130c3b46d590af91e1e7e2a0611f1e8c5" "f7fed1aadf1967523c120c4c82ea48442a51ac65074ba544a5aefc5af490893b" "e2c926ced58e48afc87f4415af9b7f7b58e62ec792659fcb626e8cba674d2065" "d6844d1e698d76ef048a53cefe713dbbe3af43a1362de81cdd3aefa3711eae0d" "846b3dc12d774794861d81d7d2dcdb9645f82423565bfb4dad01204fa322dbd5" "d268b67e0935b9ebc427cad88ded41e875abfcc27abd409726a92e55459e0d01" "0717ec4adc3308de8cdc31d1b1aef17dc61003f09cb5f058f77d49da14e809cf" "36282815a2eaab9ba67d7653cf23b1a4e230e4907c7f110eebf3cdf1445d8370" "835868dcd17131ba8b9619d14c67c127aa18b90a82438c8613586331129dda63" default))
 '(doom-modeline-mode t)
 '(inverse-video t)
 '(warning-suppress-log-types '((comp) (comp)))
 '(warning-suppress-types '((comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
