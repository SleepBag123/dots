#+title: Economics Question Bank

* Question 1 :drill:
What is the difference between final and intermediate goods
** Answer
| Characteristic        | Final                                        | Intermediate                                |
|-----------------------+----------------------------------------------+---------------------------------------------|
| Boundry of Production | Goods crossed BOP and used by consumers      | Not crossed BOP cannot be used by Consumers |
| Raw Material          | Not used as raw material for production      | Used as raw material for further production |
| Reselling             | These goods will not be resold in the market | They maybe resold by retailers              |
| Value Added           | There is no need for value addition          | Value maybe added to these goods            |
| Expenditure           | Expenditure here is called final expenditure | Expense here is intermediate consumption    |
| GDP                   | Included in calculations of GDP              | Not included in GDP calculations            |

* Question 2 :drill:
How would you recognise differences between expenditure on final and intermediate goods

** Answer
- Final expenditure consists of Consumption Expenditure or Investment Expenditure (C +I) if that is not the case for the given expense we can say that the expenditure is an example of intermediate consumption, like money spent on raw materials

* Question 3 :drill:
What is the difference between Stock and Flow

** Answer
| Stock                                                     | Flow                                                 |
|-----------------------------------------------------------+------------------------------------------------------|
| Stock refers to the value of a variable at a point of tim | Refers to value of variable over a period of time    |
| It is not time dimentional as it is at a specific point   | Time dimentional as calculated per hour, per day etc |
| Example Wealth                                            | Example Income                                       |

* Question 4 :drill:
How is consumption and production linked in a 2 sector economy
** Answer
- The Households are the owners of the *factors of production* which they provide to the producers for the process of production
a- The producers in return provide the households with factor payment in the form of rent, interest, wages or profit
- The money provided as factor income is then reused by the consumers to buy goods and services for their consumption leading to *consumption expenditure*, the producers recieve the money for the goods and services as factor income
- The money that is given to the households by the producers is then spent on the producers for utility by the households meaning that the money is moving around in circles.
- If the households donot engage in consumption expenditure then there is no income for producers, but the lack of factor income with the producer alsso means that ther is no income for the households too as they derive their income from the factor payments by the producers.
- In this way we can see that both production and consumption are linked and in absence of one the other cannot flourish

* Question 5 :drill:
Explain the problem of double counting in the value added method and how can it be solved
** Ans
- The problem of double counting arises when trying to evaluate the aggregate value of goods and services produced in the economy which is a part of calculating national income through the value added method.
- It arises when we include the value of intermediate consumption independent of the final value of the goods and services produced. Eg: If a dress has required a certain fabric to make it, adding the value of the fabric as well as the final value of the dress will lead to double counting
- This is because the value of the final goods already includes the value of the intermediate goods. The value of the dress contains the value of the fabric used to make that dress. Thus adding the value of the fabric again to the dress would mean that there is an unnessary addition to the value of national income.
- The problem of double counting is solved in the product method of calculating national income through substracting the intermediate consumption from the value of output in the economy so that we can get the ammount of *value addition* in the economy. That is a much more pragmatic approach to calculating the aggregate ammount of goods and services in the economy

* Question 6 :drill:
What are reasons due to which GDP is an ineffective measure of national income (This is a long answer)
** Ans
- It is based on an average thus does not show uniformity in distribution
- It does not take into consideration non-monetary exchanges
- It does take into consideration externalities
  
* Question 7 :drill:
Explain the idea of marginal propensity to consume and marginal propensity to save
** Ans
- Marginal Propensity to Consume:
  - Refers to the change in overall consumption levels of the consumer with a corresponding change in the income levels of the consumer
- Marginal Propensity to Save:
  - Refers to the change in the savings of the consumer with a corresponding change in the income of the consumer
    
* Question 8 :drill:
Why we consider AD = Y and explain what Y is
** Ans
- We considr AD = Y because we consider that the economy we are talking about is a 2 sector economy. Thus in 2 sector econ setup the consumption expenditure on the demand of the consumers is what the producers get as divident which is then given back to the households as factor payments for FoP thus the consumption expenditure becomes income which is why we can say that AD = Y
- Y refers to the yield or the aggregate of the income recieved in the economy

* Question 9 :drill:
Explain what is the meaning of Investment Multiplier
** Ans
Investment Multiplier or K refers to the change in the equilibrium values of aggregate demand corresponding to an initial change in autonomous investment

* Question 10                                                         :drill:
Explain the concept of the *Paradox of Thrift*
** Ans
- The Paradox of Thrift states that in an economy the amount of savings will never increase it will either reduce or remain unchanged.
- This is because when a consumer decides to increase the part of his income given to savings he at the same time is also altering his *marginal propensity to consumer* and his *income* as we already know that ones expense is the others income in a 2 sector economy
- So if the consumer wishes to increases savings his MPC declines from say 0.8 to 0.5 leading to a change in consumption from 0.8(Y) to 0.5(Y) and thus the decline in AD will also be equal to (0.8-0.5)(Y).
- The AD will reduce by the above  value but AS does not change yet and thus owing to the reduction in the value of AD, producers will not want to keep their stocks to remain unsold and thus will reduce production. A reduction in production will lead to a reduction in the factor income gained by the households ie Y.  Consumption expenditure further reduces which leads to the cycle which reduces income again.
- Thus the reduction of savings will come to be equal to the value of savings previously set as, while there is an increase in savings there is also a consequent decrease in income which cancels the changes made to the MPS and brings savings to the same level

* Question 11 :drill:
What is the meaning of a *Full Employment Level* in the Economy
** Ans
- Full Employment Level in the Economy refers to the output of the economy when everyone who wishes to work is able to and all the resources in the economy are properly utilised to their full potential
- Output in an economy cannot increase beyond the point of Full Employment level, incase the demand of the economy is above the FEL then it will considered as excess demand which cannot be fulfilled by the producers as they donot have any more resources to exploit
- Equlibrium in the economy does not also mean FEL, as it just means that left to itself the AD will equal to the AS in the economy it does not concern itself with the utilisation of resources

* Question 12 :drill:
What is the result of excess demand in the economy
** Ans
- It leads to an increase in prices
- It also leads to unfullfiled demand in the economy

* Question 13 :drill:
What is the result of deficit demand in the economy
** Ans
 - Low profits
 - Deflationary spiral (Low Demand - Low Production - Low Profit - Low Income - Low Demand)
- There is a reduction in the inducement to invest in the economy and thus the FDI reduces [[id:840a35e4-9b58-40cc-8e5b-50b8615ac3ae][(Read Sri Lankan Crisis (2022))]]

* Question 14 :drill:
List the possible ways to control excess demand and deficit demand in the economy 
** Ans
- Government Expenditure
- Tax
- Public Borrowing
- Borrowing from the RBI
- CRR and SLR
- Bank Rate and Repo Rate
- Reverse Repo Rate
- Open Market Operations
- Margin Requirements
- Moral Suations
- Credit Rationing
