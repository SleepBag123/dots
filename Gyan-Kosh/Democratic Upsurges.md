# Democratic Upsurges

## What is a Democratic Upsurge 

- The increasing participation of people in the domestic politics of the country is called a *democratic upsurge* 
- According to political scientists there 3 upsurges 
	1. 1950-1970
	2. 1980-1990
	3. 1990-2020
- The first upsurge occurred when the Universal Franchise was first applied in India in 1952 which allowed all men and women above 18 years to vote for the GOI 
- The 2nd one occurred when the dalit, OBC SC-ST agenda fist came to Indian politics with parties like Janata Party taking up the problem of reservations for OBC's in the Mandal Commission in 1979
- The 3rd came with the liberalisation of Indian economy and the beginning of the secular kerfuffles with the Shah Bano Case and the Ayodha Demolition case where leaders from both sides mobilised religious Indians into the political arena
