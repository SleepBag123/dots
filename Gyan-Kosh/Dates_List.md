# Dates List

| Event                                           | Date                                           |
|-------------------------------------------------|------------------------------------------------|
| Organisation for European Economic Co-Operation | 1948                                           |
| Council of Europe                               | 1949                                           |
| European Economic Community                     | 1957                                           |
| European Parliament                             | 1957                                           |
| ASEAN                                           | 1967                                           |
| Maldives Independence                           | 1968                                           |
| India Send Peacekeeping Forces to Sri Lanka     | 1987                                           |
| European Union                                  | 1992                                           |
| Musharraf Coup                                  | 1999                                           |
| Bhutan Constitutional Monarchy                  | 2008                                           |
| Nepal a Democratic Republic                     | 2008                                           |
| Civilian Govt in Pakistan                       | 2008                                           |
| Article 370 Abolished                           | 6 August 2019.                                 |


