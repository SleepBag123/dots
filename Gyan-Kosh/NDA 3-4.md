# NDA 3 and 4 

- Called as the end of the **Era of Coalitions** which began in 1989 with the fall of the INC and the rise of small regional parties. With the *surplus majority* that has been gained by the BJP lead NDA with BJP gaining its own majority without needing the NDA coalition members, it is the first time that a party has won a majority since 1989 almost 30 years now
- This victory was repeated in 2019 with the BJP winning 303 of its own seats and 350 total with the NDA coalition members out of the 543 members of the Lok Sabha
- Movement from the INC System in the 1950's to the 1970 to the BJP system of Politics post 2000
- Some important policies part of the regime
	- Jan Dhan Yojna
	- Swatch Bharat Yojna
	- Gati Shakti Yojna
	- Make in India
	- Skill India
	- PM Awas Yojna
	- PM Ujjawala Yojna
- Main motive of the NDA 3 and 4 governments have been to establish the idea of equality and egalitarian growth in all sectors with their moto of **"Sabka Sath, Sabka Vikas, Sabka Vishwas"**
