:PROPERTIES:
:ID:       6e967491-a666-4fc0-875f-eb56ee510f3a
:END:
#+title: Ukraine Russia War (2022)

* Main Conflict
- The conflict is about the post Cold War security arrangement in Europe
- It also about a Russian past that it wishes to relive, which is that of pre 1991 Russia
- The shared ethinic background of the people of the 2 countries has been exploited for electoral and military benefits
- Ukaine was an important buffer state between Russia and the West, and thus now its bid to be a part of the West European NATO alliance makes it a very dangerous prospect for Russian National Security
** India
- India has held a very unique position in the conflict and has neither condemned nor condoned the actions of the Russian Govt. According to several statements by the MEA Modi seems to be in active conversation with the President V.Putin and are trying to find a way of the conflict
- It has not done anything and maintained abstainence due to its need for Russian support in Central Asia and to reduce Chinese influence in the region
- India will also be affected by the conflict though as the prices of oil and petrol hit a high point
* Crimea
- Peninsula situated on the northern coast of Black Sea and the Sea of Azov
- It was part of a lot of different empires from the Greeks to the Ottomans but was annexed by the Russian Empire in 1783 as a result of the Ruso-Turkish War
- In 1954 it was transffered to the Ukrainian Sovient Socialist Republic on the decision of Nikita Khruschev
- The Revolution of Dignity lead to the oust of the Ukrainian President Viktor Yanukovych and Russian troops were deployd in Crimean govt buildings
- Crimea declared independece on 16 March 2014 leading to a disputed refferendum within the people of the state out of which 90% said they wished to be a part of Russia
- Russia officialy annexed Crimea on 18 March incorporating the Republic of Crimea as part of its federal subjects
- Currently it still considered as a part of the Ukraine by many countries displayed through a UN resolution

* G8
- The Russian Federation and the Group of 7 from the informal Group of 8 or G8
- It represents the Worlds most Industrialised countries but *Russia was removed in 2014 leading to its alleged illegal annexation of Crimean peninsula*
- It had also announced its complete and permanent oust from the Group of 8 in 2017 after it was removed in 2014
- Russia has subsequently changed its focus onto the G20 nations which include other emerging economies too such as India, Saudi and Brazil
  
* Minsk Deal
- Aimed at solving the conflict in the *Donbass* region in Ukraine
- Drafted in 2014 by the *Trilateral Contact Group on Ukraine* which included *Ukraine Russia and OSCE (Organisation For Security and Co-operation in Europe)* it included mediators from France and Germany
- Signed on *5th September 2014*
- Minsk is a city in Belarus
- The agreement was also signed by the leaders of the LPR and DPR (Luhansk and Dontesk People's Republic)
- Called for immediate ceasefire but did not end up working and the fighting did not stop
- Minsk Agreement 2 was signed in 2015 which again came with a bunch of different policies and solutions to reduce violence but still the conflict never ended properly and the provisions of the agreement were never properly implemented
- On 21st of Feb 2022 Russia and its president V.Putin recognised the LPR and DPR and following that on 22nd he also declared the Minsk agreements to no longer exist and on 24th Russia invaded Ukraine
- At the time of signing the Ukrainian President was *Petro Poroshenko*
  
* Bucha Massacre
- It was the killing of Ukrainian civilians by the Russian Arms Forces during the fight for the Ukrainian city of Bucha
- Bucha is a city part of Kyiv Oblast in Ukraine
- 412 Bodies have been recovered as per the local authorities
- Ukraine has also asked the International Court of Justice to look in the matter
   
* Chernobyl
- The disaster occured on 26 April in 1986
- The power levels of reactor 4 came to abnormally low and thus the reactor became unstable.
- After the test that was scheduled in the reactor the operators tried to shut the reactor down but instead of shutting down it lead to a nuclear chain reaction releasing energy and leading to a nuclear explosion
- It was aquired by thr Russian forces on 24th of Feb 2022 and has since then sealed the site off to make sure that there is no security concern
- Since then the IAEA has raised concern over the problems that political instability could bring to a place that is radioactively unstable
- Eventually the Russian troops withdrew from Chernobyl on 1st of April 2022

* SWIFT

* Color Revolution in Ukraine
