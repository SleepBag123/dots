#+title: Things to Remember Before Economics Exam


* National Income
- Net Domestic Fixed Capital Formation + Change In Stock = Net Domestic Capital Formation
- Gross Domestic Capital Formation - Net Domestic Capital Formation = Consumption of Fixed Capital
- Expenditure on Corporate Tax or any Tax of any sort is not included in the estimation of national income
- The payment of interest by anyone but the individual is included in the estimation of national income. This is because the individual uses the loan for his own personal use and not for productive use
- Wages and Salaries + Employers Contribution to Social Security = Compensation to Employees
- APS = Average Propensity to Save or APC = Average Propensity to Consume does not mean the average of all the savings and consumption of the consumer, just the consumption at a point of time
- Reasons for Unemployment
- Relevance of Infrastructure
- Look at Question No 9 from paper no 3 Arihant Economics
- Look at Question No 12 from paper no 2 Arihant Economics
