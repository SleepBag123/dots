# Non Congressism

- A Movement in 1967 named by Ram Manohar Lohiya 
- Aimed at united all the different political parties under one roof to make sure that the INC donot win another election
- Based on the idea that because the policies of the INC were anti-poor, the opposition must do its duty to the country and remove the incumbent govt
- It provided very fruitful results in the 1967 elections as the INC lost the provincial elections in 7 states and many INC stalwarts were not elected
- Though even this *ganging-up* against the INC could not stop it from gaining majority at the center


