# Integral Humanism

- **Indegeneous Socio-Economic Model** (important keyword)
- Based on 3 parts
	- Primacy of Whole
	- Supremacy of Dharma
	- Autonomy of Soceity
- Both Capitalism and Socialism were not up to the mark for DDU because he considered them to only focus on the material plane. He wanted something that also took mans spiritual needs into consideration  
- He put focus on the internal conscience or the pure human soul Chitti can also be related to the focus on Man by Jay Prakash Narayan in his idea of [Total Revolution](Total Revolution)
- He envisaged a classless casteless and conflict free system 
- Deen Dayal Upadhyaya advocated Indianization  of Democracy, particularly with a focus on Economic Democracy For him, decentralization & Swadeshi are the foundation of Economic Democracy.
- His philosophy broadly revolved around the principle of Arthayaam which states that both the absence and prominence of artha lead to the destruction and denigration of Dharma which is so central to Integral Humanism
