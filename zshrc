# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.

# Created by newuser for 5.8
#
neofetch
autoload -U colors && colors	# Load colors
export FZF_DEFAULT_COMMAND="find ."
export PATH="$HOME/.local/bin:$PATH"
export GOBIN="/usr/local/bin"
alias ls="exa -l"
alias nv="nvim"
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
alias lg="lazygit"
bindkey -v
setxkbmap -option caps:swapescape
autoload -U edit-command-line
zle -N edit-command-line
bindkey '^E' edit-command-line                   # Opens Vim to edit current command line
bindkey '^R' history-incremental-search-backward # Perform backward search in command line history
bindkey '^S' history-incremental-search-forward  # Perform forward search in command line history
bindkey '^P' history-search-backward             # Go back/search in history (autocomplete)
bindkey '^N' history-search-forward              # Go forward/search in history (autocomplete)
