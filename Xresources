! vim:ts=8

#ifdef HIDPI
! pkg_add xcursor-dmz
Xcursor.theme:			Simp1e
Xcursor.size:			64

Xft.dpi:			148
#endif

! Black + DarkGrey
*color0:  rgb:00/00/00
*color8:  rgb:26/26/26
! DarkRed + Red
*color1:  rgb:d7/5f/00
*color9:  rgb:d7/87/5f
! DarkGreen + Green
*color2:  rgb:af/87/5f
*color10: rgb:d7/af/87
! DarkYellow + Yellow
*color3:  rgb:ff/d7/87
*color11: rgb:ff/d7/5f
! DarkBlue + Blue
*color4:  rgb:5f/87/af
*color12: rgb:87/af/d7
! DarkMagenta + Magenta
*color5:  rgb:87/5f/5f
*color13: rgb:af/87/af
!DarkCyan + Cyan
*color6:  rgb:ff/d7/af
*color14: rgb:ff/ff/d7
! LightGrey + White
*color7:  rgb:a8/a8/a8
*color15: rgb:ff/ff/ff
! FG/BG/Cursor
URxvt*foreground:  rgb:ff/af/5f
URxvt*background:  rgb:00/00/00
URxvt*cursorColor: rgb:d7/5f/00
XTerm*foreground:  rgb:ff/af/5f
XTerm*background:  rgb:00/00/00
XTerm*cursorColor: rgb:d7/5f/00

! -----------------------------------------------------------------------------
! File: gruvbox-light.xresources
! Description: Retro groove colorscheme generalized
! Author: morhetz <morhetz@gmail.com>
! Source: https://github.com/morhetz/gruvbox-generalized
! Last Modified: 6 Sep 2014
! -----------------------------------------------------------------------------

!! hard contrast: *background: #f9f5d7
!*background: #fbf1c7
!! soft contrast: *background: #f2e5bc
!*foreground: #3c3836
!! Black + DarkGrey
!*color0:  #fdf4c1
!*color8:  #928374
!! DarkRed + Red
!*color1:  #cc241d
!*color9:  #9d0006
!! DarkGreen + Green
!*color2:  #98971a
!*color10: #79740e
!! DarkYellow + Yellow
!*color3:  #d79921
!*color11: #b57614
!! DarkBlue + Blue
!*color4:  #458588
!*color12: #076678
!! DarkMagenta + Magenta
!*color5:  #b16286
!*color13: #8f3f71
!! DarkCyan + Cyan
!*color6:  #689d6a
!*color14: #427b58
!! LightGrey + White
!*color7:  #7c6f64
!*color15: #3c3836

#ifdef HIDPI
*.font:				-*-lucida-medium-r-*-*-24-*-*-*-*-*-*-*
#else
*.font:				-*-lucida-medium-r-*-*-12-*-*-*-*-*-*-*
#endif

! xterm-specific settings

#ifdef HIDPI
*.internalBorder:		28
#else
*.internalBorder:		12
#endif
*.saveLines:			20000
*.scrollBar:			false
XTerm.rightScrollBar:		true
URxvt.scrollBar_right:		true
URxvt.scrollstyle:		xterm
*.selectToClipboard:		true
*.termName:			xterm-256color
*.utmpInhibit:			true
URxvt.loginShell:		true

! hide when i type
XTerm.pointerMode:		2
URxvt.pointerBlank:		true

! if i scrolled up, leave it that way when more output comes
URxvt.scrollTtyOutput:		false
! but not when i press a key
URxvt.scrollTtyKeypress:	true

! needed by ratpoison
XTerm.allowSendEvents:		true
! which disables allowColorOps, but empty the list of actual disabled ops
! which effectively re-enables color ops
XTerm.disallowedColorOps:

XTerm.visualBell:		false


! make alt+v/command+v paste clipboard
XTerm.vt100.translations:	#override\
    Meta <KeyPress> V: insert-selection(CLIPBOARD, PRIMARY, CUT_BUFFER0)\n\
    Super <KeyPress> V: insert-selection(CLIPBOARD, PRIMARY, CUT_BUFFER0)
URxvt.keysym.M-v:		eval:paste_clipboard
URxvt.keysym.Mod4-v:		eval:paste_clipboard
URxvt.cutchars:			"`\"'&()*,;<=>?@[]^{|}.:/-"


Xft.dpi: 96
Xft.antialias: true
Xft.hinting: true
Xft.rgba: rgb
Xft.autohint: true
Xft.hintstyle: hintfull
Xft.lcdfilter: lcdfilter


URxvt*imLocale: en_US.UTF-8
URxvt*termName: rxvt-unicode-256color
URxvt*buffered: false
URxvt.intensityStyles: false
URxvt.font: xft:FantasqueSansMono Nerd Font:size=13
URxvt.letterSpace: -1
URxvt.internalBorder:  10
URxvt.saveLines: 2000
URxvt.scrollBar: false

! support right-clicking urls to open them
URxvt.perl-ext-common:		default,matcher,selection-to-clipboard,-selection
URxvt.url-launcher:		HOME/bin/firefox
URxvt.matcher.button:		3
URxvt.matcher.rend.0:		Uline

! disable paste warning
URxvt.perl-ext:			-confirm-paste

! make xaw scrollbars act like normal ones
XTerm.*.Scrollbar.translations: #override\
    <Btn1Down>: StartScroll(Continuous) MoveThumb() NotifyThumb() \n\
    <Btn2Down>: StartScroll(Forward) \n\
    <Btn3Down>: StartScroll(Backward) \n\
    <Btn1Motion>: MoveThumb() NotifyThumb() \n\
    <BtnUp>: NotifyScroll(Proportional) EndScroll()

SshAskpass*inputTimeout:	15

! invert for qconsole
qconsole.*.background:		FG
qconsole.*.foreground:		#999999
qconsole.*.borderColor:		FG
qconsole.*.color0:		#999999

#ifdef HIDPI
SshAskpass*Button.font:		-*-lucida-medium-r-*-*-32-*-*-*-*-*-*-*
SshAskpass*Dialog.font:		-*-lucida-medium-r-*-*-36-*-*-*-*-*-*-*
SshAskpass*Indicator.width:	40
SshAskpass*Indicator.height:	20
SshAskpass*defaultXResolution:	200/in
SshAskpass*defaultYResolution:	200/in
#endif

XClock*.update:			1
XClock*.analog:			False
XClock*.render:			True
XClock*.padding:		40
XClock*.face:			Microsoft Sans Serif:size=20
XClock*.strftime:		%a %b %d  %H:%M
XClock*.background:		#c0c7c8
